FROM node:19-alpine3.16

# Nentuin file apa yang akan di set di docker
WORKDIR /usr/src/app

# copy semua package yang ekstensi json, dan di join path kedalam folder workdir docker ./
COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000

# menjlankan perintah untuk app.js
CMD ["node", "app.js"]
