const app = require("../app.js");
const request = require("supertest");

describe("GET /todos", () => {
  it("get all /todos", (done) => {
    request(app)
      .get("/todos")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        console.log(response.body);
        const data = response.body[0];
        expect(data.id).toBe(1);
        expect(data.title).toBe("Membaca Buku Programing");
        expect(data.status).toBe("active");
        done();
      })
      .catch(done);
  });

  it("get detail /todos/:id", (done) => {
    const id = 2;
    request(app)
      .get(`/todos/${id}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        console.log(response.body);
        const data = response.body;
        expect(data.id).toBe(2);
        expect(data.title).toBe("Menyelesaikan Laporan Keuangan");
        expect(data.status).toBe("active");
        done();
      })
      .catch(done);
  });

    it("create todo /todos", (done) => {
      request(app)
        .post(`/todos`)
        .send({
          title: "Menulis Diary Harian",
          status: "active",
          body: "Menulis diary harian pada jam 10 malam jangan lupa",
          tags: "diary",
        })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(201)
        .then((response) => {
          console.log(response.body.data);
          const data = response.body.data;
          expect(data.title).toBe("Menulis Diary Harian");
          expect(data.status).toBe("active");
          expect(response.body.message).toBe("created succesfuly");
          done();
        })
        .catch(done);
    });

  it("delete /todos/:id", (done) => {
    //sesuaikan dengan id yang ingin di hapus
    const id = 77;
    request(app)
      .delete(`/todos/${id}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        console.log(response.body);
        const data = response.body;
        expect(data.message).toBe('Soft Delete Succesfuly');
        done();
      })
      .catch(done);
  });
});
