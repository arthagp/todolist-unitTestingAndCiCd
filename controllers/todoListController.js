const { Todo } = require("../models");

class TodosController {

  static findAll = async (req, res) => {
    try {
      const data = await Todo.findAll({
        where:{
          status: 'active'
        }
      });
      res.status(200).json(data);
    } catch (error) {
      throw error;
    }
  };

  static findOne = async (req, res) => {
    try {
      const { id } = req.params;
      const data = await Todo.findOne({
        where: {
          id,
          status: 'active',
        },
      });
      // console.log(data);
      if (data !== null) {
        res.status(200).json(data);
      } else {
        res.status(404).json({
          message: "Not Found",
        });
      }
    } catch (error) {
      throw error;
    }
  };

  static create = async (req, res) => {
    try {
      const { title, status, body, tags } = req.body;

      const data = await Todo.create({
        title,
        status,
        body,
        tags,
      });
      res.status(201).json({
        data: data,
        message: 'created succesfuly'
      });
    } catch (error) {
      throw error;
    }
  };

  static destroy = async (req, res) => {
    try {
      const { id } = req.params;
      const data = await Todo.findOne({
        where: {
          id,
        },
      });
      if (data !== null) {
        const deleteData = await Todo.update({
          status: "inactive",
        },{
          where: {
            id,
          }
        });
        res.status(200).json({
          message: 'Soft Delete Succesfuly'
        });
      } else {
        res.status(404).json({
          message: "Id Not Found",
        });
      }
    } catch (error) {
      throw error;
    }
  };
}

module.exports = TodosController;
