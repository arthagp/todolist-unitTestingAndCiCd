const express = require('express');
const router = express.Router();
const todos = require('./todoRoutes.js');

router.use(todos)

module.exports = router