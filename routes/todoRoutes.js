const express = require('express');
const router = express.Router();
const todoController = require('../controllers/todoListController.js');

router.get('/todos', todoController.findAll);
router.get('/todos/:id', todoController.findOne);
router.post('/todos', todoController.create);
router.delete('/todos/:id', todoController.destroy);



module.exports = router