"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [
      {
        title: "Membaca Buku Programing",
        status: "active",
        body: "Membaca buku pemrograman javscript, belajar asynchornus",
        tags: "learning",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Menyelesaikan Laporan Keuangan",
        status: "active",
        body: "Menyelesaikan laporan keuangan kuartal 1 tahun 2023, termasuk membuat laporan arus kas dan neraca",
        tags: "work, finance",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Menyapu Lantai dan Membersihkan Kamar Mandi",
        status: "active",
        body: "Menyapu dan membersihkan lantai seluruh rumah, termasuk membersihkan kamar mandi dan toilet",
        tags: "housework, cleaning",
        createdAt: new Date(),
        updatedAt: new Date(),
      }
      
    ]);
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
